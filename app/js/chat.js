function animate(draw, duration) {
	const start = performance.now();
	requestAnimationFrame(function animate(time) {
		let timePassed = time - start;
		
		if (timePassed > duration) timePassed = duration;

		draw(timePassed);

		if (timePassed < duration) {
			requestAnimationFrame(animate);
		}
  });
}

Vue.component('app-chat', {
	'template': `#app-chat-template`,
	data() {
		return {
			'wait': [
					{
						'question': 'Who`s Frank?',
						'q_date': 'today, 4:20 pm',
						'answer': 'Frank is a Taxi Bot 🚕 for Facebook Messenger and Telegram,\r\n used for ordering taxis without downloading dozens of taxi apps',
						'a_date': 'today, 4:21 pm',
						'transition': '',
						'typing': false
					},
					{
						'question': 'So, Frank is another taxi company?',
						'q_date': 'today, 4:21 pm',
						'answer': 'No. Frank integrates small and medium local taxi\r\n services from around the world!',
						'a_date': 'today, 4:22 pm',
						'transition': '',
						'typing': false
					},
					{
						'question': 'Sounds great! Can I use it?',
						'q_date': 'today, 4:22 pm',
						'answer': 'Not yet. We`re now testing it with 10+ taxi companies\r\n and will fully launch the project after the crowdsale.',
						'a_date': 'today, 4:23 pm',
						'transition': '',
						'typing': false
					}
				],
			'messages': []
		};
	},

	'methods': {
		active(index) {
			let typingTime = 2000;
			let step0 = this.wait[0].question.length * 20;
			this.wait[0].transition = 'animation-delay: ' + 100 + 'ms';
			this.messages.push( this.wait[0] );
			setTimeout(()=>{
					this.messages[0].typing = true;
				}, 100)
			setTimeout(()=> {
				this.messages[0].typing = false;
			}, typingTime)

			let step = this.wait[1].question.length * 20 + 3000;
			setTimeout( () => {
				this.wait[1].transition = 'animation-delay: ' + 100 + 'ms';
				this.messages.push(this.wait[1]);
				setTimeout(()=>{
					this.messages[1].typing = true;
				}, 100)
				setTimeout(()=> {
					this.messages[1].typing = false;
				}, typingTime)
			}, step);

			let step2 = step + this.wait[1].question.length * 20 + 2500;
			setTimeout( () => {
				this.wait[2].transition = 'animation-delay: ' + 100 + 'ms';
				this.messages.push(this.wait[2]);
				setTimeout(()=>{
					this.messages[2].typing = true;
				}, 100)
				setTimeout(()=> {
					this.messages[2].typing = false;
				}, typingTime)
			}, step2);
		}
	},

	created() {
		const self = this;
		const wow = new WOW({
			'boxClass': 'chat',
			'animateClass': 'chat_show',
			'offset': 0,
			'mobile': true,
			'live': true,
			'callback': function(box) {
				self.active();
			},
			'scrollContainer': null
		});

		wow.init();
	}
});

if (document.querySelector('#app--chat')){
	const app_chat = new Vue({
		el: '#app--chat'
	});
}