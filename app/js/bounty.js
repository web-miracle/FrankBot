function animate(draw, duration) {
	const start = performance.now();
	requestAnimationFrame(function animate(time) {
		let timePassed = time - start;
		
		if (timePassed > duration) timePassed = duration;

		draw(timePassed);

		if (timePassed < duration) {
			requestAnimationFrame(animate);
		}
  });
}

Vue.component('app-bounty-cards', {
	'template': `#app-bounty-cards-template`,
	data() {
		return {
			'cards': [{
						'cost': 30,
						'description': [
								'Video Reviews &',
								'Exclusive Support'
							]
					}, {
						'cost': 25,
						'description': [
								'Articles and',
								'Written Reviews'
							]
					}, {
						'cost': 15,
						'description': [
								'Translation',
								'Campaign'
							]
					}, {
						'cost': 10,
						'description': [
								'BitCoinTalk Community',
								'Management & Signature',
								'Campaign'
							]
					}, {
						'cost': 10,
						'description': [
								'Reddit',
								'Campaign'
							]
					}, {
						'cost': 5,
						'description': [
								'Facebook',
								'Engagement'
							]
					}, {
						'cost': 5,
						'description': [
								'Twitter',
								'Engagement'
							]
					}]
		};
	},

	'methods': {
		active(index) {
			const self = this;
			this.cards
				.forEach((card) => {
					const last = card.cost;
					animate((time) => {
						card.cost = Math.ceil(time / 1000 * last);
					}, 1000);
				});
		}
	},

	created() {
		const self = this;
		const wow = new WOW({
			'boxClass': 'bounty__cards',
			'animateClass': '',
			'offset': 0,
			'mobile': true,
			'live': true,
			'callback': function(box) {
				self.active();
			},
			'scrollContainer': null
		});

		wow.init();
	}
});

if (document.querySelector('#bounty')){
	const app_bounty = new Vue({
		el: '#bounty'
	});
}