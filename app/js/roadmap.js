Vue.component('app-roadmap', {
	'template': `#app-roadmap-template`,
	data() {
		return {
			'tabs': [{
						'name': 'March, 2018 - $2,5M',
						'active': true,
						'info': [
								'Stage 1 of cryptocurrency payment system for taxi services development',
								'Testing of partnership system with selected partners',
								'Integration with taxi companies in Top-10 indicated countries during ICO'
							]
					}, {
						'name': 'October, 2018 - $15M',
						'active': false,
						'info': [
								'2 Stage 1 of cryptocurrency payment system for taxi services development',
								'2 Testing of partnership system with selected partners',
								'2 Integration with taxi companies in Top-10 indicated countries during ICO'
							]
					}, {
						'name': 'March, 2018 - $2,5M',
						'active': false,
						'info': [
								'3 Stage 1 of cryptocurrency payment system for taxi services development',
								'3 Testing of partnership system with selected partners',
								'3 Integration with taxi companies in Top-10 indicated countries during ICO'
							]
					}]
		};
	},
	'methods': {
		active(index) {
			if( !this.tabs[index].active ){
				this.tabs.forEach((item) => {
					item.active = false;
				});
				this.tabs[index].active = true;
			}
		}
	}
});

if (document.querySelector('#roadmap')){
	const app_roadmap = new Vue({
		el: '#roadmap'
	});
}