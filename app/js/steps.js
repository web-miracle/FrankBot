class Steps {
	
	constructor(node) {
		this.node = node;
		this.max_steps = 6;
		this.items = this.setItems();
		this.now = 0;
		this.tooltip = this.node.querySelector('.tooltip');
		this.tooltipInfo = [
				null,
				{
					'top': '340px',
					'left': '140px',
					'assets_class': ['tooltip__arrow', 'tooltip__arrow_top'],
					'message': 'Customer orders a taxi through <span class="tooltip__description_color">“Partner app 1”</span><br>that doesn\'t work in that location'
				},
				{
					'top': '340px',
					'left': '385px',
					'assets_class': ['tooltip__arrow', 'tooltip__arrow_top'],
					'message': '<span>“Partner app 1”</span> transfers the order to Frankly Partnership system'
				},
				{
					'top': '340px',
					'left': '625px',
					'assets_class': ['tooltip__arrow', 'tooltip__arrow_top'],
					'message': '<span>“Partner app 2”</span> that works in that location and was approved in advance by <span>“Partner app 1”</span> takes the order'
				},
				{
					'top': '0px',
					'left': '625px',
					'assets_class': ['tooltip__arrow'],
					'message': '<span>“Partner app 2”</span> pays a fee for using Frankly Partnership'
				},
				{
					'top': '0px',
					'left': '385px',
					'assets_class': ['tooltip__arrow'],
					'message': '<span>“Partner app 1”</span> receives a fee from the ride for sharing it'
				},
				{
					'top': '0px',
					'left': '140px',
					'assets_class': ['tooltip__arrow'],
					'message': 'Customer uses <span>“Partner app 1”</span> the whole time and gets a top-notch quality service!'
				}
			];

		this.nextStep();
		this.setEvents();
	}

	setItems() {
		const self = this;
		const result = [];
		this.node
			.querySelectorAll('[data-step]')
			.forEach((item) => {
				const steps = item.getAttribute('data-step').split('|');
				steps.forEach((node) => {
					if( !result[node] ){
						result[node] = [];
					}
					result[node].push(item);
					item.style.opacity = '0.25';
					item.style.filter = 'grayscale(100%)';
					item.style.transition = 'all 300ms ease';
				});
			});

			return result
	}

	setEvents() {
		const next = this.tooltip.querySelector('.tooltip__next');
		const cancel = this.tooltip.querySelector('.tooltip__cancel');
		const self = this;
		next.addEventListener('click', () => { self.nextStep(); });
		cancel.addEventListener('click', () => { self.end(); });
	}

	showStep(step) {
		if (this.now > 0 && this.now <= this.max_steps) {
			const now = this.now;
			this.items[now]
				.forEach((item) => {
					item.style.opacity = '0.25';
					item.style.filter = 'grayscale(100%)';
				});
		}

		this.items[step]
			.forEach((item) => {
				item.style.opacity = '1';
				item.style.filter = 'grayscale(0)';
			});

		this.now = step;
		this.setTooltip();
	}

	nextStep() {
		const next = this.now + 1;
		if (next <= this.max_steps) {
			this.showStep(next);
		} else {
			this.end();
		}
	}

	end() {
		this.now = this.max_steps + 1;

		this.items
			.forEach((step) => {
				step.forEach((item) => {
					item.style.opacity = '1';
					item.style.filter = 'grayscale(0)';
				});
			});

		this.setTooltip();
	}

	setTooltip() {
		if (this.now > this.max_steps || this.now < 1) {
			this.tooltip.style.display = 'none';
			return;
		}
		const info = this.tooltipInfo[this.now];
		this.tooltip.style.top = info.top;
		this.tooltip.style.left = info.left;
		const arrow = this.tooltip.querySelector('.tooltip__arrow');
		arrow.setAttribute('class', '');
		info.assets_class.forEach((arr_class) => {
			arrow.classList.add(arr_class);
		});
		this.tooltip.querySelector('.tooltip__description').innerHTML = info.message;
		this.tooltip.querySelector('.tooltip__num').innerText = this.now;
	}

}

const app = document.getElementById('app--steps-block');
if (app) {
	const stepsApp = new Steps(app);
}
