let writeLetter = function (array, nodeElement) {
	if ( array.length ) {
		nodeElement.innerHTML += array[0];
		array.shift();
	}
}

let writeText = function (nodeElement) {
	let text = nodeElement.dataset.text;
	let array = text.split('');
	const arL = array.length;
	const id = setInterval(writeLetter, 20, array, nodeElement);
	setTimeout(()=> { clearInterval(id) }, 55 * arL);
}

let contentLoaded = function() {
	
	let writtens = document.querySelectorAll('.written');
	for (var i = writtens.length - 1; i >= 0; i--) {
		let written = writtens[i];
	}

	const scrollContainer = (document.body.offsetWidth > 1200 && (document.querySelectorAll('.fullpage').length > 0) && window.innerHeight > 900) ? '.fullpage' : null;
	var wow =
		new WOW(
			{
				boxClass:     'wow',      // animated element css class (default is wow)
				animateClass: 'animated', // animation css class (default is animated)
				offset:       0,          // distance to the element when triggering the animation (default is 0)
				mobile:       true,       // trigger animations on mobile devices (default is true)
				live:         true,       // act on asynchronously loaded content (default is true)
				callback:     function(box) {
					if (box.classList.contains('written')) {
						writeText(box);
					}
				},
				scrollContainer: scrollContainer // optional scroll container selector, otherwise use window
			}
		);
	wow.init();
}

contentLoaded();


var wow2 = new WOW({
	boxClass:     'graph',          // animated element css class (default is wow)
	animateClass: 'animate', // animation css class (default is animated)
	offset:       0,                        // distance to the element when triggering the animation (default is 0)
	mobile:       true,                     // trigger animations on mobile devices (default is true)
	live:         true,                     // act on asynchronously loaded content (default is true)
	callback:     function(box) {},
	scrollContainer: null // optional scroll container selector, otherwise use window
});

wow2.init();

const headerLogo = document.querySelector('.nav__logo-greeting');
writeText(headerLogo);