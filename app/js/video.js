function initVideoForClick(){
	let videos = document.querySelectorAll('[data-youtubeVideo]');

	for (let i = 0; i < videos.length; i++) {
		let video = videos[i];
		const videoWidth = video.offsetWidth;
		// video.style.height = ( ( videoWidth / 4 ) * 3 ) + 'px';
		const youtubeKey = video.getAttribute('data-youtubeVideo') || false;

		if( youtubeKey ){
			video.style.backgroundImage = 'url(https://i1.ytimg.com/vi/' + youtubeKey + '/hqdefault.jpg)';
			video.addEventListener( 'click', () => {
				video.setAttribute('data-active', 'active');

				let iframe = document.createElement('iframe');
				iframe.setAttribute("src", "https://www.youtube.com/embed/" + youtubeKey + "?autoplay=1&start=0&controls=0&rel=0&showinfo=0");
				iframe.style.width = '100%';
				iframe.style.height = '100%';
				iframe.setAttribute('frameborder', 0);
				iframe.setAttribute('type', 'text/html');
				iframe.setAttribute('allowfullscreen', '');
				
				video.innerHTML = '';
				video.appendChild(iframe);
			});
		}
	}
}

initVideoForClick();