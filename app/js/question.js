Vue.component('app-single-question', {
	'template': `#app-one-question`,
	'props' : {
		'question': {
			'type': String
		},
		'q_date':  {
			'type': String
		},
		'answer': {
			'type': String
		},
		'a_date': {
			'type': String
		},
		'to_next_node': {
			'type': String
		},
		'to_next_class': {
			'type': String
		}
	},
	data: function() {
		return {
			'wait': [{
						'question': this.question,
						'q_date': this.q_date,
						'answer': this.answer,
						'a_date': this.a_date,
						'transition': '',
						'show': false
					}],
			'messages': [],
			'next': this.to_next_node
		};
	},

	'methods': {
		active() {
			this.wait[0].transition = 'animation-delay: ' + (this.wait[0].question.length * 20 + 400) + 'ms';
			this.messages[0] = this.wait[0];
			// if (document.body.offsetWidth > 1200) {
				const next = document.querySelector('.' + this.to_next_node);
				if (next) {
					next.style.animationDelay = (this.wait[0].question.length * 20 + 500) + 'ms';
				}
			// }
		}
	},

	created() {
		const self = this;
		const scroll = (document.body.offsetWidth > 1200 && window.innerHeight > 900) ? '.fullpage' : null;
		const wow = new WOW({
			'boxClass': 'app--message',
			'animateClass': 'app--message_show',
			'offset': 0,
			'mobile': true,
			'live': true,
			'callback': function(box) {
				self.active();
			},
			'scrollContainer': scroll
		});

		if (self.to_next_class && self.to_next_node) {
			const wow2 = new WOW({
				boxClass:     self.to_next_node,
				animateClass: self.to_next_class,
				offset:       0,
				mobile:       true,
				live:         true,
				callback:     function(box) {},
				scrollContainer: scroll
			});

			wow2.init();
		}

		wow.init();
	}
});

if (document.querySelector('#app--steps')){
	const app_steps = new Vue({
		el: '#app--steps'
	});
}

if (document.querySelector('#app--about-us-problems')){
	const app_about_us_problems = new Vue({
		el: '#app--about-us-problems'
	});
}

if (document.querySelector('#app--about-us-success')){
	const app_about_us_success = new Vue({
		el: '#app--about-us-success'
	});
}

if (document.querySelector('#app--about-us-using')){
	const app_about_us_using = new Vue({
		el: '#app--about-us-using'
	});
}

if (document.querySelector('#app--about-us-contacts')){
	const app_about_us_contacts = new Vue({
		el: '#app--about-us-contacts'
	});
}