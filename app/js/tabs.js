Vue.component('app-problems', {
	'template': `#app-problems-template`,
	data() {
		return {
			'tabs': [{
						'name': 'Background',
						'active': true,
						'info': [
								'Frank Taxi Bot and Frankly Partnership have been in development since 2015 and has already attracted sufficient funds from private investors.',
								'We have developed a fully operational taxi ordering system and taxi order sharing system!'
							]
					}, {
						'name': 'Problems',
						'active': false,
						'info': [
								'2 Frank Taxi Bot and Frankly Partnership have been in development since 2015 and has already attracted sufficient funds from private investors.',
								'2 We have developed a fully operational taxi ordering system and taxi order sharing system!'
							]
					}, {
						'name': 'Solutions',
						'active': false,
						'info': [
								'3 Frank Taxi Bot and Frankly Partnership have been in development since 2015 and has already attracted sufficient funds from private investors.',
								'3 We have developed a fully operational taxi ordering system and taxi order sharing system!'
							]
					}]
		};
	},
	'methods': {
		active(index) {
			if( !this.tabs[index].active ){
				this.tabs.forEach((item) => {
					item.active = false;
				});
				this.tabs[index].active = true;
			}
		}
	}
});

if (document.querySelector('#problems')){
	const app_problems = new Vue({
		el: '#problems'
	});
}