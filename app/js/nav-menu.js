document.addEventListener('scroll', (event) => {
	const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
	const menu = document.querySelector('nav.nav');
	
	if (scrollTop > 300 && menu) {
		menu.nextElementSibling.style.paddingTop = menu.offsetHeight + 'px';
		menu.classList.add('nav--scroll');
	} else if (menu) {
		menu.nextElementSibling.style.paddingTop = '0px';
		menu.classList.remove('nav--scroll');
	}
});