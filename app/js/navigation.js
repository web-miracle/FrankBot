function animate(draw, duration) {
	const start = performance.now();
	requestAnimationFrame(function animate(time) {
		let timePassed = time - start;
		
		if (timePassed > duration) timePassed = duration;
		draw(timePassed);

		if (timePassed < duration) {
			requestAnimationFrame(animate);
		}
  });
}

let contentLoaded2 = function () {
	let navs = document.querySelectorAll('.navigation');
	if (navs) {
		for (var i = navs.length - 1; i >= 0; i--) {
			let nav = navs[i];
			nav.addEventListener('click', (e) => {
				e.preventDefault();
				let target = document.querySelector('#' + nav.dataset.nav);
				// let startPos = nav.scrollTop;
				// console.log(startPos);
				// let endPos = target.offsetTop;
				// animate((time)=>{
				// 	let step = startPos + (endPos - startPos) / 500 * time;
				// 	window.scroll(0, step);
				// }, 500);
				
				if (target) {
					window.scroll({
						behavior: 'smooth',
						left: 0,
						top: target.offsetTop
					});
				}
			});
		}
	}
}

window.addEventListener('DOMContentLoaded', contentLoaded2);