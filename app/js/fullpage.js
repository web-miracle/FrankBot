function addWheelEvent() {
	const screen = document.querySelectorAll('.fullpage__item');

	screen.forEach((elem) => {
		if (elem.addEventListener) {
			if ('onwheel' in document) {
				elem.addEventListener("wheel", onWheel);
			} else if ('onmousewheel' in document) {
				elem.addEventListener("mousewheel", onWheel);
			} else {
				elem.addEventListener("MozMousePixelScroll", onWheel);
			}
		} else {
			elem.attachEvent("onmousewheel", onWheel);
		}
		
		document.addEventListener('keyup', onWheel);
	});
}

function removeWheelEvent() {
	const screen = document.querySelectorAll('.fullpage__item');

	screen.forEach((elem) => {
		if (elem.removeEventListener) {
			if ('onwheel' in document) {
				elem.removeEventListener("wheel", onWheel);
			} else if ('onmousewheel' in document) {
				elem.removeEventListener("mousewheel", onWheel);
			} else {
				elem.removeEventListener("MozMousePixelScroll", onWheel);
			}
		} else {
			elem.detachEvent("onmousewheel", onWheel);
		}

		document.removeEventListener('keyup', onWheel);
	});	
}

function onWheel(e) {
	e = e || window.event;
	const delta = e.deltaY || e.detail || e.wheelDelta || e;
	console.log(delta);
	const fullpage = document.querySelector('.fullpage');
	const pagesCount = fullpage.scrollHeight - fullpage.offsetHeight;
	const height = fullpage.offsetHeight;

	if (delta > 0 || ("key" in e && delta.key.toLowerCase() === "arrowdown")) {
		let startPos = fullpage.scrollTop;
		if (pagesCount > startPos) {
			removeWheelEvent();
			let endPos = startPos + height;
			
			animate((time)=>{
				let step = startPos + (endPos - startPos) / 250 * time;

				if (step > startPos) {
					fullpage.scroll(0, step);
				}
			}, 250);
			
			setTimeout(() => {
				addWheelEvent();
			}, 250);
		}
	} else if (delta < 0 || ("key" in e && delta.key.toLowerCase() === "arrowup")) {
		let startPos = fullpage.scrollTop;
		if (startPos > 0) {
			removeWheelEvent();
			let endPos = startPos - height;
			
			animate((time)=>{
				let step = startPos + (endPos - startPos) / 250 * time;
				if ( step < startPos ) {
					fullpage.scroll(0, step);
				}
			}, 250);
			
			setTimeout(() => {
				addWheelEvent();
			}, 250);
		}
	}
}

function animate(draw, duration) {
	const start = performance.now();
	requestAnimationFrame(function animate(time) {
		let timePassed = time - start;
		
		if (timePassed > duration) timePassed = duration;
		draw(timePassed);

		if (timePassed < duration) {
			requestAnimationFrame(animate);
		}
	});
}

if (document.body.offsetWidth > 1200 && (document.querySelectorAll('.fullpage').length > 0) && window.innerHeight > 900) {
	addWheelEvent();
}
