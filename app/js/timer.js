Vue.component('app-timer', {
	props : {
		todate : {
			type: String
		}
	},
	'template': `<span>ICO in <strong>{{ days | two_digits }}</strong>&nbsp;days
			<i class="timer__separator">:</i>
			<strong>{{ hours | two_digits }}</strong>&nbsp;hours
			<i class="timer__separator">:</i>
			<strong>{{ minutes | two_digits }}</strong>&nbsp;minutes
			<i class="timer__separator">:</i>
			<strong>{{ seconds | two_digits }}</strong>&nbsp;seconds</span>`,
	data() {
		return {
			date: Math.round((new Date(this.todate)).getTime() / 1000),
			now: Math.round((new Date()).getTime() / 1000)
		}
	},
	created() {
		window.setInterval(() => {
			this.now = Math.round((new Date()).getTime() / 1000);
		}, 1000);
	},
	'computed': {
		seconds() {
			return (this.date - this.now) % 60;
		},

		minutes() {
			return Math.round((this.date - this.now) / 60) % 60;
		},

		hours() {
			return Math.round((this.date - this.now) / 60 / 60) % 24;
		},

		days() {
			return Math.round((this.date - this.now) / 60 / 60 / 24);
		}
	}
});

Vue.filter('two_digits', function (value) {
	if(value.toString().length <= 1)
	{
		return "0"+value.toString();
	}
	return value.toString();
});

if (document.querySelector('#timer')){
	const app = new Vue({
		el: '#timer'
	});
}