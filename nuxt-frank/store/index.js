import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = () => new Vuex.Store({
  state: {
    scrollMenu: false,
    activeSolution: false
  },
  mutations: {
    toScroll(state, val) {
      state.scrollMenu = !!val;
    },
    changeSolution(state, val) {
      state.activeSolution = !!val;
    }
  }
});

export default store
