import Vue from 'vue';

function writeLetter(array, nodeElement) {
	if (array.length) {
		nodeElement.innerHTML += array[0];
		array.shift();
	}
}

function writeText(nodeElement) {
	const text = nodeElement.dataset.text;
	const array = text.split('');
	const arL = array.length;
	const interval = setInterval(writeLetter, 20, array, nodeElement);
	setTimeout(()=> {
		clearInterval(interval);
	}, 55 * arL);

	return interval;
}

Vue.directive('written', {
	inserted(nodeElement, binding) {
		if (process.browser) {
			let WOW = require('~/plugins/wow.js').WOW;
			const addClass = 'wow-' + Math.floor(Math.random() * 10000);
			const animateClass = (binding.value) ? binding.value.animateClass || 'animated' : 'animated';
			const scrollContainer = (binding.value) ? binding.value.scrollContainer || null : null;
			nodeElement.classList.add(addClass);
			
			const wow = new WOW({
				boxClass:     addClass,
				animateClass: animateClass,
				offset:       0,
				mobile:       true,
				live:         true,
				callback:     function(box) {
					writeText(box);
				},
				"scrollContainer": scrollContainer
			});

			nodeElement.wowInit = wow;
			wow.init();
		}
	}
});

export { writeText };