import Vue from 'vue';

Vue.directive('wow', {
	inserted(nodeElement, binding) {
		if (process.browser) {
			let WOW = require('~/plugins/wow.js').WOW;
			const addClass = 'wow-' + Math.floor(Math.random() * 10000);
			const animateClass = (binding.value) ? binding.value.animateClass || 'animated' : 'animated';
			const callback = (binding.value) ? binding.value.callback || function(){} : function(){};
			const scrollContainer = (binding.value) ? binding.value.scrollContainer || null : null;
			nodeElement.classList.add(addClass);
			const offset = (binding.value) ? binding.value.offset || 0 : 0;
			
			const wow = new WOW({
				"boxClass":     addClass,
				"animateClass": animateClass,
				"offset":       offset,
				"mobile":       true,
				"live":         true,
				"scrollContainer": scrollContainer,
				"callback": callback
			});

			nodeElement.wowInit = wow;
			wow.init();
		}
	},
	unbind(nodeElement) {
		if (nodeElement.wowInit) {
			nodeElement.wowInit.stop();
		}
	}
});