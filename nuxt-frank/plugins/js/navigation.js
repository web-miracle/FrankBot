import Vue from 'vue';

Vue.directive('navigation', {
	inserted(nodeElement, binding) {
		if (process.browser) {
			const self = this;
			nodeElement.listenerFunc = () => {
				let scrollTop = 0;
				if (document.body.offsetWidth > 1200 && window.innerHeight > 900) {
					scrollTop = document.querySelector('.fullpage').scrollTop;
				} else {
					scrollTop = window.pageYOffset || document.documentElement.scrollTop;
				}

				const menu = nodeElement;
				
				if (scrollTop > 300 && menu) {
					if (menu.parentNode.nextElementSibling)
						menu.parentNode.nextElementSibling.style.paddingTop = menu.offsetHeight + 'px';
	
					menu.classList.add('nav--scroll');
				} else if (menu) {
					if (menu.parentNode.nextElementSibling)
						menu.parentNode.nextElementSibling.style.paddingTop = '0px';
					
					menu.classList.remove('nav--scroll');
				}
			};
			
			if (document.body.offsetWidth > 1200 && window.innerHeight > 900) {
				document.querySelector('.fullpage').addEventListener('scroll', nodeElement.listenerFunc);
			} else {
				document.addEventListener('scroll', nodeElement.listenerFunc);
			}
		}
	},
	unbind(nodeElement) {
		if (process.browser) {
			
			if (document.body.offsetWidth > 1200 && window.innerHeight > 900) {
				document.querySelector('.fullpage').removeEventListener('scroll', nodeElement.listenerFunc);
			} else {
				document.removeEventListener('scroll', nodeElement.listenerFunc);
			}
		}
	}
});