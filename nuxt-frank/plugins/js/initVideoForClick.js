import Vue from 'vue';	
import './wow.js';
import './written.js';
import './navigation.js';
import './scroll-to.js';
import './fullpage.js';
import { Steps } from './steps.js';

function initVideoForClick(video, youtubeKey){
	// const videoWidth = video.offsetWidth;
	// video.style.height = ( ( videoWidth / 4 ) * 3 ) + 'px';

	if (video && youtubeKey) {
		video.setAttribute('y-key', youtubeKey);
		video.style.backgroundImage = 'url(https://i1.ytimg.com/vi/' + youtubeKey + '/hqdefault.jpg)';
		video.addEventListener('click', () => {
				video.setAttribute('data-active', 'active');

				let iframe = document.createElement('iframe');
				iframe.setAttribute("src", "https://www.youtube.com/embed/" + youtubeKey + "?autoplay=1&start=0&controls=0&rel=0&showinfo=0");
				iframe.style.width = '100%';
				iframe.style.height = '100%';
				iframe.setAttribute('frameborder', 0);
				iframe.setAttribute('type', 'text/html');
				iframe.setAttribute('allowfullscreen', '');
				
				video.innerHTML = '';
				video.appendChild(iframe);
			}, {"once": true}
		);
	}
}

function bindEvent(video, youtubeKey) {
	video.setAttribute('data-active', 'active');

	let iframe = document.createElement('iframe');
	iframe.setAttribute("src", "https://www.youtube.com/embed/" + youtubeKey + "?autoplay=1&start=0&controls=0&rel=0&showinfo=0");
	iframe.style.width = '100%';
	iframe.style.height = '100%';
	iframe.setAttribute('frameborder', 0);
	iframe.setAttribute('type', 'text/html');
	iframe.setAttribute('allowfullscreen', '');
	
	video.innerHTML = '';
	video.appendChild(iframe);
}

Vue.directive('youtube-video', {
	bind(nodeElement, binding) {
		if (process.browser) {
			initVideoForClick(nodeElement, binding.value.key);
		}
	}
});

Vue.directive('steps', {
	bind(nodeElement, binding) {
		if (process.browser) {
			const stepsApp = new Steps(nodeElement);			
		}
	}
});