import Vue from 'vue';

Vue.directive('scroll-to', {
	bind(nodeElement, binding) {
		if (process.browser) {
			const self = this;
			nodeElement.listenerFunc = (event) => {
				if ($nuxt.$route.name !== 'index')
					return;
				event.preventDefault();

				if (!$nuxt.$store.state.scrollMenu) {
					const target = document.getElementById(binding.value.to);
					if (target) {
						$nuxt.$store.commit("toScroll", true);

						const container = (document.body.offsetWidth > 1200 && window.innerHeight > 900) ? document.querySelector('.fullpage') : window;

						container.scroll({
							behavior: 'smooth',
							left: 0,
							top: target.offsetTop
						});

						setTimeout(function() {
                            $nuxt.$store.commit("toScroll", false)
                        }, 700)
					}
				}
			};
			nodeElement.addEventListener("click", nodeElement.listenerFunc);
		}
	},
	unbind(nodeElement) {
		if (process.browser) {
			nodeElement.removeEventListener("click", nodeElement.listenerFunc);
		}
	}
});