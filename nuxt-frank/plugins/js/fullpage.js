import { animate } from "~/plugins/js/animate.js";
import Vue from 'vue';

Vue.directive('fullpage', {
	inserted(nodeElement, binding) {
		if (process.browser && document.body.offsetWidth > 1200 && window.innerHeight > 900) {
			addWheelEvent(nodeElement);
		}
	},
	unbind(nodeElement) {
		if (process.browser && nodeElement.listenerFunc) {
			removeWheelEvent(nodeElement);
		}
	}
});

function addWheelEvent(nodeElement) {
	const screen = nodeElement.querySelectorAll('.fullpage__item');

	screen.forEach((elem) => {
		if (elem.addEventListener) {
			if ('onwheel' in document) {
				elem.addEventListener("wheel", onWheel);
			} else if ('onmousewheel' in document) {
				elem.addEventListener("mousewheel", onWheel);
			} else {
				elem.addEventListener("MozMousePixelScroll", onWheel);
			}
		} else {
			elem.attachEvent("onmousewheel", onWheel);
		}
	});
	
	document.addEventListener('keyup', onWheel);
}

function removeWheelEvent(nodeElement) {
	const screen = nodeElement.querySelectorAll('.fullpage__item');

	screen.forEach((elem) => {
		if (elem.removeEventListener) {
			if ('onwheel' in document) {
				elem.removeEventListener("wheel", onWheel);
			} else if ('onmousewheel' in document) {
				elem.removeEventListener("mousewheel", onWheel);
			} else {
				elem.removeEventListener("MozMousePixelScroll", onWheel);
			}
		} else {
			elem.detachEvent("onmousewheel", onWheel);
		}
	});

	document.removeEventListener('keyup', onWheel);
}

function onWheel(e) {
	e = e || window.event;
	const delta = e.deltaY || e.detail || e.wheelDelta || e;
	const fullpage = document.querySelector('.fullpage');
	const pagesCount = fullpage.scrollHeight - fullpage.offsetHeight;
	const height = fullpage.offsetHeight;

	let item = e.target;
	while (!item.classList.contains('fullpage__item')) {
		item = item.parentNode;
	}

	if ($nuxt.$store.state.activeSolution) {
		return;
	}

	if (
		(delta > 0 && item.scrollTop >= item.scrollHeight - item.clientHeight)
		||
		("key" in e && delta.key.toLowerCase() === "arrowdown")
	) {
		let startPos = fullpage.scrollTop;
		if (pagesCount > startPos) {
			removeWheelEvent(fullpage);
			let endPos = startPos + height;
			
			animate((time)=>{
				let step = startPos + (endPos - startPos) / 250 * time;

				if (step > startPos) {
					fullpage.scroll(0, step);
				}
			}, 250);
			
			setTimeout(() => {
				addWheelEvent(fullpage);
			}, 250);
		}
	} else if (
		(delta < 0 && item.scrollTop === 0)
		||
		("key" in e && delta.key.toLowerCase() === "arrowup")
	) {
		let startPos = fullpage.scrollTop;
		if (startPos > 0) {
			removeWheelEvent(fullpage);
			let endPos = startPos - height;
			
			animate((time)=>{
				let step = startPos + (endPos - startPos) / 250 * time;
				if ( step < startPos ) {
					fullpage.scroll(0, step);
				}
			}, 250);
			
			setTimeout(() => {
				addWheelEvent(fullpage);
			}, 250);
		}
	}
}
