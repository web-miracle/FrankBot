webpackJsonp([1], {
    "1f8K":function(t, s, a) {
        "use strict";
        var e=function() {
            var t=this, s=t.$createElement, a=t._self._c||s;
            return a("div", {
                staticClass:"about-us-using", attrs: {
                    id: "app--about-us-using"
                }
            }
            , [a("div", {
                staticClass: "container"
            }
            , [a("div", {
                staticClass: "row"
            }
            , [a("div", {
                staticClass: "col-12"
            }
            , [a("div", {
                staticClass: "about-us-using__message"
            }
            , [a("app-single-question", {
                attrs: {
                    question: "That's neat! But why bother? Can't I just use Uber?", q_date: "today, 4:32 pm", answer: "Uber is not everywhere and the number of countries it works in is only decreasing.\n\nBesides, such huge international corporations share a monopoly of taxi business\namong themselves, which leaves small regional taxi companies in dirt, with no\nchance of independent development or even survival.", a_date: "today, 4:33 pm", to_next_node: "chat__about-us-success2", to_next_class: "chat__about-us-success2_show"
                }
            }
            , [a("div", {
                staticClass: "container"
            }
            , [a("div", {
                staticClass: "row"
            }
            , [a("div", {
                staticClass: "col-12"
            }
            , [a("ul", {
                staticClass: "chat__about-us-success2"
            }
            , [a("li", {
                staticClass: "chat__about-us-success_title order-md-1"
            }
            , [t._v("\n\t\t\t\t\t\t\t\tBy using Frankly Partnership, "), a("b", [t._v("Taxi Companies")]), t._v(":\n\t\t\t\t\t\t\t")]), a("li", {
                staticClass: "chat__about-us-success_white order-md-2"
            }
            , [a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#check-green"
                }
            }
            )]), a("span", [t._v("\n\t\t\t\t\t\t\t\t\twill have a way to compete with "), a("br"), t._v("\n\t\t\t\t\t\t\t\t\tsuch ride-sharing moguls;\n\t\t\t\t\t\t\t\t")])]), a("li", {
                staticClass: "chat__about-us-success_white order-md-2"
            }
            , [a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#check-green"
                }
            }
            )]), a("span", [t._v("\n\t\t\t\t\t\t\t\t\tcan support your local "), a("br"), t._v("\n\t\t\t\t\t\t\t\t\ttaxi companies;\n\t\t\t\t\t\t\t\t")])]), a("li", {
                staticClass: "chat__about-us-success_title order-md-1"
            }
            , [t._v("\n\t\t\t\t\t\t\t\tand "), a("b", [t._v("You")]), t._v(":\n\t\t\t\t\t\t\t")]), a("li", {
                staticClass: "chat__about-us-success_white order-md-2"
            }
            , [a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#check-green"
                }
            }
            )]), a("span", [t._v("\n\t\t\t\t\t\t\t\t\twon't sacrifice their brand and "), a("br"), t._v("\n\t\t\t\t\t\t\t\t\tindependence for survival;\n\t\t\t\t\t\t\t\t")])]), a("li", {
                staticClass: "chat__about-us-success_white order-md-2"
            }
            , [a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#check-green"
                }
            }
            )]), a("span", [t._v("\n\t\t\t\t\t\t\t\t\thelp the industry remain "), a("br"), t._v("\n\t\t\t\t\t\t\t\t\tfair and competitive.\n\t\t\t\t\t\t\t\t")])]), a("svg", {
                staticClass: "chat__about-us-success_line hide-sm"
            }
            , [a("use", {
                attrs: {
                    "xlink:href": "#line"
                }
            }
            )])])])])])])], 1)])])])])
        }
        ;
        e._withStripped=!0;
        var i= {
            render: e, staticRenderFns: []
        }
        ;
        s.a=i
    }
    , "1fhg":function(t, s, a) {
        "use strict";
        var e=function() {
            var t=this, s=t.$createElement, a=t._self._c||s;
            return a("div", {
                staticClass: "lang"
            }
            , [a("div", {
                staticClass:"lang__current", class: {
                    "lang__current--list-open": t.listOpen
                }
                , on: {
                    click:function(s) {
                        t.open()
                    }
                }
            }
            , [a("img", {
                staticClass:"lang__img", attrs: {
                    src: t.currentLang.image, alt: t.currentLang.name, title: t.currentLang.name
                }
            }
            ), a("button", {
                staticClass: "lang__trigl"
            }
            )]), a("transition", {
                attrs: {
                    name: "fadeInDown"
                }
            }
            , [t.listOpen?a("ul", {
                staticClass: "lang__list lang__list--active"
            }
            , t._l(t.langList, function(s) {
                return a("li", {
                    key:s.name, staticClass:"lang__item", on: {
                        click:function(a) {
                            t.active(s)
                        }
                    }
                }
                , [a("img", {
                    staticClass:"lang__img", attrs: {
                        src: s.image, alt: s.name, title: s.name
                    }
                }
                )])
            }
            )):t._e()])], 1)
        }
        ;
        e._withStripped=!0;
        var i= {
            render: e, staticRenderFns: []
        }
        ;
        s.a=i
    }
    , "3Os3":function(t, s, a) {
        (t.exports=a("FZ+f")(!1)).push([t.i, ".chat{padding-top:70px}.chat--big{min-height:460px}.chat__block{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;-ms-flex-wrap:nowrap;flex-wrap:nowrap;margin-bottom:20px}.chat__block_hide,.chat__block_show{opacity:0}.chat__question{font-family:Proxima Nova;font-size:18px;font-weight:700;font-style:normal;font-stretch:normal;line-height:1.61;letter-spacing:normal;text-align:left;color:#fed91f;-ms-flex-item-align:end;align-self:flex-end;margin-bottom:0}.chat__question--dark{color:#672440}.chat__question+.chat__date{-ms-flex-item-align:end;align-self:flex-end}.chat__date{opacity:.4;font-size:8px;font-weight:700;line-height:2;text-transform:uppercase}.chat__answer,.chat__date{font-family:Proxima Nova;font-style:normal;font-stretch:normal;letter-spacing:normal;text-align:left;color:#fff;margin-bottom:0}.chat__answer{font-size:18px;font-weight:400;line-height:1.44;-ms-flex-item-align:start;align-self:flex-start;white-space:pre-line}.chat--big .chat__answer{font-size:21px}@media screen and (max-width:1299px){.chat__answer{white-space:normal}}.chat__answer+.chat__date{-ms-flex-item-align:start;align-self:flex-start}.chat__about-us-problems,.chat__about-us-success,.chat__about-us-success2{padding:0;margin:0;margin-top:100px;list-style:none;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row;-ms-flex-wrap:wrap;flex-wrap:wrap;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between}.chat__about-us-problems_show,.chat__about-us-success2_show,.chat__about-us-success_show{-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation-duration:.5s;animation-duration:.5s;-webkit-animation-name:fadeInLeft;animation-name:fadeInLeft}@media screen and (max-width:767px){.chat__about-us-problems,.chat__about-us-success,.chat__about-us-success2{margin-top:40px}}.chat__about-us-problems li,.chat__about-us-success2 li,.chat__about-us-success li{width:calc(50% - 50px);margin-bottom:40px;color:#fff;padding-left:50px;font-family:Proxima Nova;font-size:18px;font-weight:700;font-style:italic;font-stretch:normal;line-height:1.44;letter-spacing:normal;text-align:left;color:#672440;position:relative}@media screen and (max-width:767px){.chat__about-us-problems li,.chat__about-us-success2 li,.chat__about-us-success li{width:100%}}.chat__about-us-problems li>svg,.chat__about-us-success2 li>svg,.chat__about-us-success li>svg{width:30px;height:30px;position:absolute;top:0;left:0}.chat__about-us-success,.chat__about-us-success2{margin-top:60px;position:relative}.chat__about-us-success2 li,.chat__about-us-success li{line-height:1.44;color:#043930}.chat__about-us-success2 li.chat__about-us-success2_title,.chat__about-us-success2 li.chat__about-us-success_title,.chat__about-us-success li.chat__about-us-success2_title,.chat__about-us-success li.chat__about-us-success_title{font-family:Proxima Nova;font-size:18px;font-style:normal;font-stretch:normal;text-align:left;padding:0;color:#50e3c2}.chat__about-us-success2 li.chat__about-us-success2_white,.chat__about-us-success2 li.chat__about-us-success_white,.chat__about-us-success li.chat__about-us-success2_white,.chat__about-us-success li.chat__about-us-success_white{font-family:Proxima Nova;font-size:18px;font-weight:700;font-style:normal;font-stretch:normal;line-height:1.44;letter-spacing:normal;text-align:left;color:#fff;margin-bottom:60px}@media screen and (max-width:567px){.chat__about-us-success2 li.chat__about-us-success2_white span br,.chat__about-us-success2 li.chat__about-us-success_white span br,.chat__about-us-success li.chat__about-us-success2_white span br,.chat__about-us-success li.chat__about-us-success_white span br{display:none}}.chat__about-us-success2_line,.chat__about-us-success_line{position:absolute;left:45%;width:30px;bottom:.75em;height:calc(100% - 2em);color:#4b5053}@media screen and (max-width:1299px){.chat_show{min-height:575px}.chat_show.chat--big{min-height:450px}}@media screen and (max-width:567px){.chat_show{min-height:675px}}.app--message_show .chat__block_show,.chat_show .chat__block_show{-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation-duration:.5s;animation-duration:.5s;-webkit-animation-name:fadeInDown;animation-name:fadeInDown}@-webkit-keyframes fadeInDown{0%{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}to{opacity:1;-webkit-transform:translateZ(0);transform:translateZ(0)}}@keyframes fadeInDown{0%{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}to{opacity:1;-webkit-transform:translateZ(0);transform:translateZ(0)}}", ""])
    }
    , "5GZ6":function(t, s, a) {
        "use strict";
        var e=a("EerU");
        s.a= {
            components: {
                "app-single-question": e.a
            }
        }
    }
    , "5HDz":function(t, s, a) {
        "use strict";
        var e=a("KkBP"), i=a("KNuB"), n=!1;
        var o=function(t) {
            n||a("DNP6")
        }
        , r=a("VU/8")(e.a, i.a, !1, o, null, null);
        r.options.__file="components/about-us/AppAboutSuccess.vue", s.a=r.exports
    }
    , "5nBj":function(t, s, a) {
        var e=a("jpGv");
        "string"==typeof e&&(e=[[t.i, e, ""]]), e.locals&&(t.exports=e.locals);
        a("rjj0")("ea11075e", e, !1, {
            sourceMap: !1
        }
        )
    }
    , "5r9R":function(t, s) {
        t.exports="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAQCAYAAAAWGF8bAAAAAXNSR0IArs4c6QAAAo5JREFUOBGVU01rE1EUPTPzJlPSQGiUGhsrgxQUBLtwJyKF7goiCAULRXDR/yLiRlx1XwRBtGuLUHAlaMFdiy3MpsVIokmmjZnMmxnPe5M3RkWpl5A378655577MRZoKyuPzwohrpfLriWlRJIAnidwEklcRQ9LwQ6kAv7NEjs7evv6w22ELUGMLVx7/eJs/W6z+RXl8iRcV6DdDtFoTKP/JcPsnSWklmLTfzwzdaHld8eyMbN27yXuLy8rQrfTOZ6fv+ahVjufw4hrNKoITwboDSMktRmkOtgQadiIMENmMUDK+bm5OUphmkTGaZKoUiUsvrRti88J0jSlFlKpAGXZ6MxvuUDl43uSphektEjoI4q+o9PpolqdRBwPSZaiUpnA4ec20OvhZO8AbCstJ6Sm/KYSZRkc28aQAgJ6SRjAcSzUpibR6YbwJlw9kFbrG85NV9EKQ1QXb0KqSY0IzaFZSSgcB1Ecw3+2rgh9JknxfucTwrDPgThMYKPfj1Cm4vpxF0cbLyDH25cL1bNRIgWHEhmFCws+nj58gJJbYl9/LocCpiTx2MMKB6PbZ0gNoZJIH1cOETF+8BEi2N5G99ETlNiHhEqLsnQ9bCFLaqoa85/2Gl7dSvqF6SG5hO/7qK+twitRoe6TimFIETVi/sfhUuFgOIT/bosKCTx8/ooKBSTXRBuz/k74h2vM4VJhlMp8yj5jp27dgOe6WqFeCaVONVFbLtUaG61ZG9UHhSqmvLWZKwx39x3peYi52IUVhMpjWvBrEp10tDaDQeQERIogCOJ0unZQipNLNhfamOEjXttYhQZSnC73sF+t7CsunXIVuHzFPbPIj85IKMCneeDnme3G7TcbwN5p8P+F+QGpYAryGhvb3gAAAABJRU5ErkJggg=="
    }
    , "78t0":function(t, s, a) {
        "use strict";
        var e=function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("nav", {
                directives:[ {
                    name: "navigation", rawName: "v-navigation"
                }
                ], staticClass:"row align-items-center nav", attrs: {
                    id: "app-nav"
                }
            }
            , [s("nuxt-link", {
                staticClass:"col-lg-3 col-md-12 nav__logo", attrs: {
                    to: "/"
                }
            }
            , [s("svg", {
                staticClass: "nav__logo-img"
            }
            , [s("use", {
                attrs: {
                    "xlink:href": "#logo"
                }
            }
            )]), s("span", {
                staticClass:"nav__logo-greeting", attrs: {
                    "data-text": "Hi, I’m FrankTaxi Bot! 🚖"
                }
            }
            )]), s("div", {
                staticClass: "col-lg-9 col-md-12 nav__menu"
            }
            , [s("nuxt-link", {
                staticClass:"nav__menu-link ml-auto", attrs: {
                    to: "/about-us"
                }
            }
            , [this._v("\n\t\t\tAbout\n\t\t")]), s("a", {
                directives:[ {
                    name:"scroll-to", rawName:"v-scroll-to", value: {
                        to: "ico"
                    }
                    , expression:"{'to': 'ico'}"
                }
                ], staticClass:"nav__menu-link navigation", attrs: {
                    href: "#"
                }
            }
            , [this._v("\n\t\t\tICO\n\t\t")]), s("a", {
                directives:[ {
                    name:"scroll-to", rawName:"v-scroll-to", value: {
                        to: "bounty"
                    }
                    , expression:"{'to': 'bounty'}"
                }
                ], staticClass:"nav__menu-link navigation", attrs: {
                    href: "#"
                }
            }
            , [this._v("\n\t\t\tBounty\n\t\t")]), s("a", {
                directives:[ {
                    name:"scroll-to", rawName:"v-scroll-to", value: {
                        to: "roadmap"
                    }
                    , expression:"{'to': 'roadmap'}"
                }
                ], staticClass:"nav__menu-link navigation", attrs: {
                    href: "#"
                }
            }
            , [this._v("\n\t\t\tRoadmap\n\t\t")]), s("a", {
                directives:[ {
                    name:"scroll-to", rawName:"v-scroll-to", value: {
                        to: "team"
                    }
                    , expression:"{'to': 'team'}"
                }
                ], staticClass:"nav__menu-link navigation", attrs: {
                    href: "#"
                }
            }
            , [this._v("\n\t\t\tTeam\n\t\t")]), s("a", {
                directives:[ {
                    name:"scroll-to", rawName:"v-scroll-to", value: {
                        to: "contact"
                    }
                    , expression:"{'to': 'contact'}"
                }
                ], staticClass:"nav__menu-link navigation", attrs: {
                    href: "#"
                }
            }
            , [this._v("\n\t\t\tContact us\n\t\t")]), s("a", {
                directives:[ {
                    name:"scroll-to", rawName:"v-scroll-to", value: {
                        to: "white"
                    }
                    , expression:"{'to': 'white'}"
                }
                ], staticClass:"nav__menu-link navigation", attrs: {
                    href: "#"
                }
            }
            , [this._v("\n\t\t\tWhite paper\n\t\t")]), s("app-lang", {
                staticClass: "nav__lang ml-auto"
            }
            )], 1)], 1)
        }
        ;
        e._withStripped=!0;
        var i= {
            render: e, staticRenderFns: []
        }
        ;
        s.a=i
    }
    , CRqv:function(t, s, a) {
        var e=a("wYaz");
        "string"==typeof e&&(e=[[t.i, e, ""]]), e.locals&&(t.exports=e.locals);
        a("rjj0")("7b41912c", e, !1, {
            sourceMap: !1
        }
        )
    }
    , CYmg:function(t, s, a) {
        "use strict";
        var e=function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("div", {
                directives:[ {
                    name: "fullpage", rawName: "v-fullpage"
                }
                ], staticClass:"fullpage"
            }
            , [s("div", {
                staticClass: "fullpage__item"
            }
            , [s("app-about-header")], 1), s("div", {
                staticClass: "fullpage__item"
            }
            , [s("app-steps")], 1), s("div", {
                staticClass: "fullpage__item"
            }
            , [s("app-about-problem")], 1), s("div", {
                staticClass: "fullpage__item"
            }
            , [s("app-about-success")], 1), s("div", {
                staticClass: "fullpage__item"
            }
            , [s("app-about-using")], 1), s("div", {
                staticClass: "fullpage__item"
            }
            , [s("div", {
                staticClass: "fullpage__two"
            }
            , [s("app-about-contact"), s("app-footer")], 1)]), s("img", {
                staticClass:"arrow--down", attrs: {
                    src: a("arnn")
                }
            }
            )])
        }
        ;
        e._withStripped=!0;
        var i= {
            render: e, staticRenderFns: []
        }
        ;
        s.a=i
    }
    , DAz3:function(t, s, a) {
        "use strict";
        var e=a("gmQo"), i=a("1fhg"), n=!1;
        var o=function(t) {
            n||a("VTNN")
        }
        , r=a("VU/8")(e.a, i.a, !1, o, null, null);
        r.options.__file="components/AppLang.vue", s.a=r.exports
    }
    , DNP6:function(t, s, a) {
        var e=a("vAh8");
        "string"==typeof e&&(e=[[t.i, e, ""]]), e.locals&&(t.exports=e.locals);
        a("rjj0")("5c8c4e3a", e, !1, {
            sourceMap: !1
        }
        )
    }
    , EZ99:function(t, s, a) {
        "use strict";
        var e=a("Xxa5"), i=a.n(e), n=a("exGp"), o=a.n(n), r=a("//Fk"), c=a.n(r);
        s.a= {
            props: {
                inputMessages: {
                    type: Array
                }
            }
            , data:function() {
                return {
                    wait:[ {
                        question: "Who`s Frank?", q_date: "today, 4:20 pm", answer: "Frank is a Taxi Bot 🚕 for Facebook Messenger and Telegram,\r\n used for ordering taxis without downloading dozens of taxi apps", a_date: "today, 4:21 pm", transition: "", typing: !1
                    }
                    , {
                        question: "So, Frank is another taxi company?", q_date: "today, 4:21 pm", answer: "No. Frank integrates small and medium local taxi\r\n services from around the world!", a_date: "today, 4:22 pm", transition: "", typing: !1
                    }
                    , {
                        question: "Sounds great! Can I use it?", q_date: "today, 4:22 pm", answer: "Not yet. We`re now testing it with 10+ taxi companies\r\n and will fully launch the project after the crowdsale.", a_date: "today, 4:23 pm", transition: "", typing: !1
                    }
                    ], messages:[], isActive:!1, typingTime:1e3, oneStageTime:2e3
                }
            }
            , methods: {
                active:function(t) {
                    this.isActive=!0
                }
                , stage1:function() {
                    var t=this;
                    return new c.a(function(s, a) {
                        t.wait[0].transition="animation-delay: 100ms", t.messages.push(t.wait[0]), t.messages[0].typing=!0, setTimeout(function() {
                            t.messages[0].typing=!1
                        }
                        , t.typingTime), setTimeout(function() {
                            s(), t.$emit("firstMessage")
                        }
                        , t.oneStageTime)
                    }
                    )
                }
                , stage2:function() {
                    var t=this;
                    return new c.a(function(s, a) {
                        t.wait[1].transition="animation-delay: 100ms", t.messages.push(t.wait[1]), t.messages[1].typing=!0, setTimeout(function() {
                            t.messages[1].typing=!1
                        }
                        , t.typingTime), setTimeout(function() {
                            s(), t.$emit("secondMessage")
                        }
                        , t.oneStageTime)
                    }
                    )
                }
                , stage3:function() {
                    var t=this;
                    return new c.a(function(s, a) {
                        t.wait[2].transition="animation-delay: 100ms", t.messages.push(t.wait[2]), t.messages[2].typing=!0, setTimeout(function() {
                            t.messages[2].typing=!1
                        }
                        , t.typingTime), setTimeout(function() {
                            s(), t.$emit("endChat")
                        }
                        , t.oneStageTime)
                    }
                    )
                }
            }
            , watch: {
                isActive:function() {
                    var t=o()(i.a.mark(function t() {
                        return i.a.wrap(function(t) {
                            for(;
                            ;
                            )switch(t.prev=t.next) {
                                case 0:if(!this.isActive) {
                                    t.next=7;
                                    break
                                }
                                return t.next=3, this.stage1();
                                case 3:return t.next=5, this.stage2();
                                case 5:return t.next=7, this.stage3();
                                case 7:case"end":return t.stop()
                            }
                        }
                        , t, this)
                    }
                    ));
                    return function() {
                        return t.apply(this, arguments)
                    }
                }
                ()
            }
            , created:function() {
                this.inputMessages&&(this.wait=this.inputMessages)
            }
        }
    }
    , EerU:function(t, s, a) {
        "use strict";
        var e=a("i70V"), i=a("aOAY"), n=a("VU/8")(e.a, i.a, !1, null, null, null);
        n.options.__file="components/AppQuestion.vue", s.a=n.exports
    }
    , FiW2:function(t, s, a) {
        "use strict";
        var e=a("RjAS"), i=a("DAz3");
        s.a= {
            components: {
                AppLang: i.a
            }
            , data:function() {
                return {
                    componentNode: null, intervalId: null
                }
            }
            , mounted:function() {
                if(this.componentNode=document.getElementById("app-nav"), this.componentNode) {
                    var t=this.componentNode.querySelector(".nav__logo-greeting");
                    this.intervalId=Object(e.a)(t)
                }
            }
            , destroyed:function() {
                clearInterval(self.intervalId)
            }
        }
    }
    , Fu4c:function(t, s, a) {
        t.exports=a.p+"img/elements@3x.d182695.png"
    }
    , G33O:function(t, s, a) {
        "use strict";
        var e=function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("header", [s("div", {
                staticClass: "container-fluid app--header"
            }
            , [s("app-nav")], 1), s("div", {
                staticClass:"container header--home", staticStyle: {
                    position: "relative"
                }
            }
            , [s("img", {
                staticClass:"about-us-header__back", attrs: {
                    src: a("G5K+"), srcset: a("kidr")+" 2x, "+a("Fu4c")+" 3x"
                }
            }
            ), s("div", {
                staticClass:"row about-us-header", attrs: {
                    id: "app--chat"
                }
            }
            , [s("div", {
                staticClass: "col-sm-12 col-md-5 about-us-header__image"
            }
            , [s("img", {
                attrs: {
                    src: a("s6Fc"), srcset: a("W4oR")+" 2x, "+a("cWjJ")+" 3x"
                }
            }
            ), s("video", {
                attrs: {
                    autoplay: "", loop: "", muted: ""
                }
                , domProps: {
                    muted: !0
                }
            }
            , [s("source", {
                attrs: {
                    src: a("tnYm"), type: "video/mp4"
                }
            }
            )])]), s("app-chat", {
                staticClass: "col-sm-12 col-md-7"
            }
            )], 1)])])
        }
        ;
        e._withStripped=!0;
        var i= {
            render: e, staticRenderFns: []
        }
        ;
        s.a=i
    }
    , "G5K+":function(t, s, a) {
        t.exports=a.p+"img/elements.e9ab066.png"
    }
    , H3me:function(t, s, a) {
        t.exports=a.p+"img/taxi.224f09e.svg"
    }
    , HVu9:function(t, s, a) {
        "use strict";
        var e=a("dPaJ"), i=a("d9O0"), n=a("VU/8")(e.a, i.a, !1, null, null, null);
        n.options.__file="components/about-us/AppSteps.vue", s.a=n.exports
    }
    , I7D9:function(t, s, a) {
        var e=a("fE+W");
        "string"==typeof e&&(e=[[t.i, e, ""]]), e.locals&&(t.exports=e.locals);
        a("rjj0")("139f0026", e, !1, {
            sourceMap: !1
        }
        )
    }
    , "J+iT":function(t, s, a) {
        "use strict";
        var e=a("O6am"), i=a("WdEC");
        s.a= {
            components: {
                "app-nav": e.a, "app-chat": i.a
            }
        }
    }
    , KNuB:function(t, s, a) {
        "use strict";
        var e=function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("div", {
                staticClass:"about-us-success", attrs: {
                    id: "app--about-us-success"
                }
            }
            , [s("div", {
                staticClass: "container"
            }
            , [s("div", {
                staticClass: "row position-relative"
            }
            , [s("img", {
                staticClass:"about-us-success__bg", attrs: {
                    src: a("gjRl")
                }
            }
            ), this._m(0), s("div", {
                staticClass: "col-lg-7 col-md-12"
            }
            , [s("div", {
                staticClass: "about-us-success__message"
            }
            , [s("app-single-question", {
                attrs: {
                    question: "Вопрос?", q_date: "today, 4:30 pm", answer: "With Frankly Partnership:", a_date: "today, 4:31 pm", to_next_node: "chat__about-us-success", to_next_class: "chat__about-us-success_show"
                }
            }
            , [s("ul", {
                staticClass: "chat__about-us-success"
            }
            , [s("li", [s("svg", {
                staticClass: "chat__svg"
            }
            , [s("use", {
                attrs: {
                    "xlink:href": "#check-green"
                }
            }
            )]), s("span", [this._v("\n\t\t\t\t\t\t\t\t\tNo need to switch to a different app;\n\t\t\t\t\t\t\t\t")])]), s("li", [s("svg", {
                staticClass: "chat__svg"
            }
            , [s("use", {
                attrs: {
                    "xlink:href": "#check-green"
                }
            }
            )]), s("span", [this._v("\n\t\t\t\t\t\t\t\t\tNo need to spend time on looking for a good taxi service;\n\t\t\t\t\t\t\t\t")])]), s("li", [s("svg", {
                staticClass: "chat__svg"
            }
            , [s("use", {
                attrs: {
                    "xlink:href": "#check-green"
                }
            }
            )]), s("span", [this._v("\n\t\t\t\t\t\t\t\t\tKnow how much you'll spend on the ride; \n\t\t\t\t\t\t\t\t")])]), s("li", [s("svg", {
                staticClass: "chat__svg"
            }
            , [s("use", {
                attrs: {
                    "xlink:href": "#check-green"
                }
            }
            )]), s("span", [this._v("\n\t\t\t\t\t\t\t\t\tNo additional fees.\n\t\t\t\t\t\t\t\t")])]), s("li", {
                staticClass: "w-100"
            }
            , [s("a", {
                staticClass:"signup-btn signup-btn_yellow position-relative", attrs: {
                    href: "/register1"
                }
            }
            , [this._v("\n\t\t\t\t\t\t\t\t\tSign up for a White List\n\t\t\t\t\t\t\t\t\t"), s("svg", {
                staticClass: "signup-btn__arrow"
            }
            , [s("use", {
                attrs: {
                    "xlink:href": "#login-arrow"
                }
            }
            )])])])])])], 1)])])])])
        }
        , i=[function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("div", {
                staticClass: "col-5 hide-md d-flex align-items-center"
            }
            , [s("img", {
                staticClass:"mw-100", attrs: {
                    src: a("cRzj")
                }
            }
            )])
        }
        ];
        e._withStripped=!0;
        var n= {
            render: e, staticRenderFns: i
        }
        ;
        s.a=n
    }
    , KkBP:function(t, s, a) {
        "use strict";
        var e=a("EerU");
        s.a= {
            components: {
                "app-single-question": e.a
            }
        }
    }
    , MdyI:function(t, s, a) {
        (t.exports=a("FZ+f")(!1)).push([t.i, ".lang{-ms-flex-align:center;-ms-flex-item-align:center;align-self:center;position:relative}.lang,.lang__current{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;align-items:center}.lang__current{margin:auto;background:rgba(0,0,0,.31);border-radius:5px;padding:8px;-ms-flex-align:center}.lang__current:hover{cursor:pointer}.lang__current--list-open .lang__trigl{-webkit-transform:rotate(180deg);transform:rotate(180deg);margin-top:-3px}.lang__img{width:20px;height:auto}.lang__trigl{border:3px solid transparent;border-top:6px solid #fed91f;background:transparent;outline:none!important;padding:0;margin-left:5px;-ms-flex-item-align:center;align-self:center;margin-top:3px;-webkit-transition:all .2s ease;transition:all .2s ease}.lang__trigl:hover{cursor:pointer}.lang__list{list-style:none;padding:0;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;-ms-flex-wrap:nowrap;flex-wrap:nowrap;-webkit-box-align:center;-ms-flex-align:center;align-items:center;margin:0;position:absolute;top:100%}.lang__item,.lang__list{width:100%;display:-webkit-box;display:-ms-flexbox;display:flex}.lang__item{text-align:center;padding:8px;background:rgba(0,0,0,.31);-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center}.lang__item:hover{cursor:pointer}.fadeInDown-enter-active{-webkit-animation:fadeInDown-in .2s;animation:fadeInDown-in .2s}.fadeInDown-leave-active{animation:fadeInDown-in .3s reverse}@-webkit-keyframes fadeInDown-in{0%{-webkit-transform:translateY(-20%);transform:translateY(-20%);opacity:0}to{-webkit-transform:translateY(0);transform:translateY(0);opacity:1}}@keyframes fadeInDown-in{0%{-webkit-transform:translateY(-20%);transform:translateY(-20%);opacity:0}to{-webkit-transform:translateY(0);transform:translateY(0);opacity:1}}", ""])
    }
    , "Ng6+":function(t, s, a) {
        "use strict";
        var e=a("WlZ6"), i=a("NgpP"), n=!1;
        var o=function(t) {
            n||a("mup7")
        }
        , r=a("VU/8")(e.a, i.a, !1, o, null, null);
        r.options.__file="components/about-us/AppAboutContact.vue", s.a=r.exports
    }
    , NgpP:function(t, s, a) {
        "use strict";
        var e=function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("div", {
                staticClass:"about-us-contacts", attrs: {
                    id: "app--about-us-contacts"
                }
            }
            , [s("div", {
                staticClass: "container"
            }
            , [s("div", {
                staticClass: "row"
            }
            , [s("div", {
                staticClass: "col-12"
            }
            , [s("div", {
                staticClass: "about-us-contacts__message"
            }
            , [s("app-single-question", {
                attrs: {
                    question: "You have my vote! When can I finally use it?", q_date: "today, 4:34 pm", answer: "Frankly Partnership will be launched in Q4 2018. For updates\nand more specific information, subscribe to our newsletter:", a_date: "today, 4:35 pm", to_next_node: "signup-btn", to_next_class: "signup-btn_show"
                }
            }
            , [s("a", {
                staticClass:"signup-btn wow signup-btn_yellow", attrs: {
                    href: "/register1"
                }
            }
            , [this._v("\n\t\t\t\t\t\t\tSign up for a White List\n\t\t\t\t\t\t\t"), s("svg", {
                staticClass: "signup-btn__arrow"
            }
            , [s("use", {
                attrs: {
                    "xlink:href": "#login-arrow"
                }
            }
            )])])])], 1)])])])])
        }
        ;
        e._withStripped=!0;
        var i= {
            render: e, staticRenderFns: []
        }
        ;
        s.a=i
    }
    , "O+Hr":function(t, s, a) {
        "use strict";
        var e=function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("footer", {
                staticClass: "footer-holder"
            }
            , [s("div", {
                staticClass: "container"
            }
            , [s("div", {
                staticClass: "footer"
            }
            , [s("div", {
                staticClass: "footer__logo"
            }
            , [s("nuxt-link", {
                staticClass:"footer__logo-link", attrs: {
                    to: "/"
                }
            }
            , [s("svg", [s("use", {
                attrs: {
                    "xlink:href": "#logo"
                }
            }
            )])]), this._m(0)], 1), this._m(1), s("div", {
                staticClass: "footer__soc-links"
            }
            , [s("a", {
                attrs: {
                    href: "#"
                }
            }
            , [s("svg", [s("use", {
                attrs: {
                    "xlink:href": "#bitcoin"
                }
            }
            )])]), s("a", {
                attrs: {
                    href: "#"
                }
            }
            , [s("svg", [s("use", {
                attrs: {
                    "xlink:href": "#soc-twitter"
                }
            }
            )])]), s("a", {
                attrs: {
                    href: "#"
                }
            }
            , [s("svg", [s("use", {
                attrs: {
                    "xlink:href": "#reddit"
                }
            }
            )])]), s("a", {
                attrs: {
                    href: "#"
                }
            }
            , [s("svg", [s("use", {
                attrs: {
                    "xlink:href": "#telegram"
                }
            }
            )])]), s("a", {
                attrs: {
                    href: "#"
                }
            }
            , [s("svg", [s("use", {
                attrs: {
                    "xlink:href": "#soc-fb"
                }
            }
            )])])]), this._m(2)])])])
        }
        , i=[function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("p", [s("span", {
                staticClass: "footer__logo-cpy"
            }
            , [this._v("\n\t\t\t\t\t\tCopyright © 2018. All Rights Reserved.\n\t\t\t\t\t")]), s("span", {
                staticClass: "footer__logo-email"
            }
            , [this._v("\n\t\t\t\t\t\tEmail:\n\t\t\t\t\t\t"), s("a", {
                attrs: {
                    href: "mailto:info@franktaxibot.com"
                }
            }
            , [this._v("\n\t\t\t\t\t\t\tinfo@franktaxibot.com\n\t\t\t\t\t\t")])])])
        }
        , function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("div", {
                staticClass: "footer__docs"
            }
            , [s("a", {
                attrs: {
                    href: "#"
                }
            }
            , [this._v("\n\t\t\t\t\tPrivacy policy\n\t\t\t\t")]), s("a", {
                attrs: {
                    href: "#"
                }
            }
            , [this._v("\n\t\t\t\t\tTerms of us\n\t\t\t\t")])])
        }
        , function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("div", {
                staticClass: "footer__ltd"
            }
            , [s("img", {
                attrs: {
                    src: a("r3k2")
                }
            }
            ), s("p", [this._v("\n\t\t\t\t\tThis website belongs to Frank Taxi Bot Ltd., Registration No. 10371414W\n\t\t\t\t")])])
        }
        ];
        e._withStripped=!0;
        var n= {
            render: e, staticRenderFns: i
        }
        ;
        s.a=n
    }
    , O6am:function(t, s, a) {
        "use strict";
        var e=a("FiW2"), i=a("78t0"), n=!1;
        var o=function(t) {
            n||a("ZSum")
        }
        , r=a("VU/8")(e.a, i.a, !1, o, null, null);
        r.options.__file="components/AppNav.vue", s.a=r.exports
    }
    , OqR1:function(t, s, a) {
        "use strict";
        var e=a("WEbS"), i=a("oT4F"), n=a("5HDz"), o=a("q55T"), r=a("Ng6+"), c=a("HVu9"), l=a("smZX");
        s.a= {
            components: {
                "app-about-header": e.a, "app-steps": c.a, "app-about-problem": i.a, "app-about-success": n.a, "app-about-using": o.a, "app-about-contact": r.a, "app-footer": l.a
            }
        }
    }
    , QQKy:function(t, s, a) {
        (t.exports=a("FZ+f")(!1)).push([t.i, ".nav{padding:1.875em 5px 0;position:relative;max-width:1400px;margin:0 auto;z-index:10}@media screen and (max-width:991px){.nav{-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column}}.nav__logo{text-decoration:none}@media screen and (max-width:1299px){.nav__logo{max-width:100%;-ms-flex-preferred-size:100%;flex-basis:100%;text-align:center}}@media screen and (max-width:991px){.nav__logo{margin-bottom:15px}}.nav__logo-img{display:inline-block;vertical-align:middle;width:3.125em;height:3.125em;margin-right:1.25em}.nav__logo-greeting{color:#fff;font-size:1.125em;font-family:Proxima Nova;vertical-align:middle}.nav__menu{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between}@media screen and (max-width:1299px){.nav__menu{max-width:100%;-ms-flex-preferred-size:100%;flex-basis:100%;padding-left:65px}}@media screen and (max-width:991px){.nav__menu{-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-ms-flex-wrap:wrap;flex-wrap:wrap;padding-left:15px}}.nav__menu-link{color:#e56ea0;font-size:.75em;font-family:Proxima Nova;font-weight:700;text-align:center;padding:15px 30px;-webkit-transition:all .2s ease;transition:all .2s ease}.nav__menu-link:hover{color:#d92d75}@media screen and (max-width:991px){.nav__menu-link:first-of-type{margin-left:0!important}}@media screen and (max-width:767px){.nav__menu-link{padding:10px}}.nav--scroll{position:fixed;max-width:unset;left:0;right:0;top:0;padding-top:10px;padding-bottom:10px;z-index:1000;background-color:rgba(0,0,0,.4);-webkit-animation-duration:.5s;animation-duration:.5s;-webkit-animation-name:fadeInDown;animation-name:fadeInDown;-webkit-transition:all .3s ease;transition:all .3s ease}.nav--scroll:hover{background-color:rgba(0,0,0,.8)}@media screen and (max-width:991px){.nav--scroll .nav__logo{margin-bottom:5px;text-align:center}}@media screen and (max-width:991px){.nav__lang{position:absolute!important;top:-55px;right:15px}}", ""])
    }
    , VBPW:function(t, s, a) {
        (t.exports=a("FZ+f")(!1)).push([t.i, ".about-us-contacts{background-color:#351e32;padding:90px 0 100px;height:100%}@media screen and (max-width:767px){.about-us-contacts{padding:20px 0}}", ""])
    }
    , VTNN:function(t, s, a) {
        var e=a("MdyI");
        "string"==typeof e&&(e=[[t.i, e, ""]]), e.locals&&(t.exports=e.locals);
        a("rjj0")("1bcb0508", e, !1, {
            sourceMap: !1
        }
        )
    }
    , W4oR:function(t, s, a) {
        t.exports=a.p+"img/iphone@2x.bbe30dc.png"
    }
    , WEbS:function(t, s, a) {
        "use strict";
        var e=a("J+iT"), i=a("G33O"), n=!1;
        var o=function(t) {
            n||a("CRqv")
        }
        , r=a("VU/8")(e.a, i.a, !1, o, null, null);
        r.options.__file="components/about-us/AppAboutHeader.vue", s.a=r.exports
    }
    , WbMt:function(t, s, a) {
        var e=a("lL8I");
        "string"==typeof e&&(e=[[t.i, e, ""]]), e.locals&&(t.exports=e.locals);
        a("rjj0")("46407a37", e, !1, {
            sourceMap: !1
        }
        )
    }
    , WdEC:function(t, s, a) {
        "use strict";
        var e=a("EZ99"), i=a("ruQK"), n=!1;
        var o=function(t) {
            n||a("kNwo")
        }
        , r=a("VU/8")(e.a, i.a, !1, o, null, null);
        r.options.__file="components/AppChat.vue", s.a=r.exports
    }
    , WlZ6:function(t, s, a) {
        "use strict";
        var e=a("EerU");
        s.a= {
            components: {
                "app-single-question": e.a
            }
        }
    }
    , ZSum:function(t, s, a) {
        var e=a("QQKy");
        "string"==typeof e&&(e=[[t.i, e, ""]]), e.locals&&(t.exports=e.locals);
        a("rjj0")("79fa53f0", e, !1, {
            sourceMap: !1
        }
        )
    }
    , aOAY:function(t, s, a) {
        "use strict";
        var e=function() {
            var t=this, s=t.$createElement, a=t._self._c||s;
            return a("div", {
                directives:[ {
                    name:"wow", rawName:"v-wow", value: {
                        animateClass: "app--message_show", callback: t.active, scrollContainer: t.wowContainer
                    }
                    , expression:"{'animateClass': 'app--message_show', 'callback': active, 'scrollContainer': wowContainer}"
                }
                ], staticClass:"app--message"
            }
            , [t._l(t.wait, function(s, e) {
                return[a("div", {
                    key: "block-"+e, staticClass: "chat__block"
                }
                , [a("p", {
                    directives:[ {
                        name:"written", rawName:"v-written", value: {
                            scrollContainer: t.wowContainer
                        }
                        , expression:"{'scrollContainer': wowContainer}"
                    }
                    ], staticClass:"chat__question wow", class:t.questionClasses, attrs: {
                        "data-text": s.question
                    }
                }
                ), a("p", {
                    staticClass: "chat__date"
                }
                , [t._v(t._s(s.q_date))])]), a("div", {
                    key:"message-"+e, staticClass:"chat__block chat__block_show animated", class: {
                        chat__block_hide: !s.show
                    }
                    , style:s.transition
                }
                , [a("p", {
                    staticClass: "chat__answer"
                }
                , [t._v(t._s(s.answer))]), a("p", {
                    staticClass: "chat__date"
                }
                , [t._v(t._s(s.a_date))])]), t._t("default")]
            }
            )], 2)
        }
        ;
        e._withStripped=!0;
        var i= {
            render: e, staticRenderFns: []
        }
        ;
        s.a=i
    }
    , arnn:function(t, s) {
        t.exports="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMCIgaGVpZ2h0PSIzMyIgdmlld0JveD0iMCAwIDMwIDMzIj4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0ibm9uemVybyI+CiAgICAgICAgPHBhdGggZmlsbD0iI0U1NkVBMCIgZD0iTTE1IDVDNy44MiA1IDIgMTAuODIgMiAxOHM1LjgyIDEzIDEzIDEzIDEzLTUuODIgMTMtMTNTMjIuMTggNSAxNSA1em0xNSAxM2MwIDguMjg0LTYuNzE2IDE1LTE1IDE1LTguMjg0IDAtMTUtNi43MTYtMTUtMTVDMCA5LjcxNiA2LjcxNiAzIDE1IDNjOC4yODQgMCAxNSA2LjcxNiAxNSAxNXoiLz4KICAgICAgICA8cGF0aCBmaWxsPSIjRkVEOTFGIiBkPSJNMTYuMjUgMjAuOTQzbDMuNy0zLjdMMjEuNzA3IDE5IDE1IDI1LjcwNyA4LjI5MyAxOWwxLjc1Ny0xLjc1NyAzLjcgMy43Vi41aDIuNXoiLz4KICAgIDwvZz4KPC9zdmc+Cg=="
    }
    , cRzj:function(t, s, a) {
        t.exports=a.p+"img/about-us-success.42de389.png"
    }
    , cWjJ:function(t, s, a) {
        t.exports=a.p+"img/iphone@3x.6b4e591.png"
    }
    , d9O0:function(t, s, a) {
        "use strict";
        var e=function() {
            var t=this, s=t.$createElement, a=t._self._c||s;
            return a("div", {
                staticClass:"steps", attrs: {
                    id: "app--steps"
                }
            }
            , [a("div", {
                staticClass: "steps__container"
            }
            , [a("app-single-question", {
                attrs: {
                    question: "What's Frankly Partnership?", q_date: "today, 4:24 pm", answer: "Frankly Partnership allows taxi services to exchange orders all around the globe   🌍", a_date: "today, 4:25 pm", to_next_node: "style-scroll", to_next_class: "style-scroll_show"
                }
            }
            )], 1), a("div", {
                staticClass: "style-scroll animated"
            }
            , [a("div", {
                directives:[ {
                    name: "steps", rawName: "v-steps"
                }
                ], staticClass:"steps-block", attrs: {
                    id: "app--steps-block"
                }
            }
            , [t._m(0), a("div", {
                staticClass: "steps-block__header"
            }
            , [a("div", {
                staticClass:"step-1", attrs: {
                    "data-step": "1"
                }
            }
            , [t._m(1), a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#arrow-1"
                }
            }
            )])]), a("div", {
                staticClass:"step-2", attrs: {
                    "data-step": "2|3"
                }
            }
            , [t._m(2), a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#arrow-2"
                }
            }
            )])])]), a("div", {
                staticClass: "steps-block__body"
            }
            , [a("div", {
                staticClass:"step-1", attrs: {
                    "data-step": "1|6"
                }
            }
            , [a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#client"
                }
            }
            )])]), a("div", {
                staticClass:"step-1-next", attrs: {
                    "data-step": "1|6"
                }
            }
            , [a("p", [t._v("\n\t\t\t\t\t\tЗаказ\n\t\t\t\t\t")]), a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#arrow-order-1"
                }
            }
            )])]), a("div", {
                staticClass:"step-2", attrs: {
                    "data-step": "1|2|5|6"
                }
            }
            , [a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#group-51"
                }
            }
            )]), a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#partner-app"
                }
            }
            )]), a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#logo"
                }
            }
            )]), t._m(3), t._m(4)]), a("div", {
                staticClass:"step-2-next", attrs: {
                    "data-step": "2"
                }
            }
            , [a("p", [t._v("\n\t\t\t\t\t\tЗаказ\n\t\t\t\t\t")]), a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#arrow-order-1"
                }
            }
            )])]), a("div", {
                staticClass:"step-3", attrs: {
                    "data-step": "2|3|4|5"
                }
            }
            , [a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#partnership-2"
                }
            }
            )])]), a("div", {
                staticClass:"step-3-next", attrs: {
                    "data-step": "3"
                }
            }
            , [a("p", [t._v("\n\t\t\t\t\t\tЗаказ\n\t\t\t\t\t")]), a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#arrow-order-1"
                }
            }
            )])]), a("div", {
                staticClass:"step-4", attrs: {
                    "data-step": "3|4"
                }
            }
            , [a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#group-51"
                }
            }
            )]), a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#taxi-soft"
                }
            }
            )]), t._m(5), t._m(6)]), a("div", {
                staticClass:"step-4-next", attrs: {
                    "data-step": "3"
                }
            }
            , [a("p", [t._v("\n\t\t\t\t\t\tЗаказ\n\t\t\t\t\t")]), a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#arrow-order-1"
                }
            }
            )])]), t._m(7)]), a("div", {
                staticClass: "steps-block__footer"
            }
            , [a("div", {
                staticClass:"step-5", attrs: {
                    "data-step": "4"
                }
            }
            , [t._m(8), a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#arrow-3"
                }
            }
            )])]), a("div", {
                staticClass:"step-6", attrs: {
                    "data-step": "5"
                }
            }
            , [t._m(9), a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#arrow-3"
                }
            }
            )])]), a("div", {
                staticClass: "step-all"
            }
            , [t._m(10), a("svg", [a("use", {
                attrs: {
                    "xlink:href": "#arrow-big"
                }
            }
            )])])])])])])
        }
        , i=[function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("div", {
                staticClass: "steps-block__tooltip tooltip"
            }
            , [s("div", {
                staticClass: "tooltip__content"
            }
            , [s("p", {
                staticClass: "tooltip__num"
            }
            , [this._v("1")]), s("div", {
                staticClass: "tooltip__message"
            }
            , [s("p", {
                staticClass: "tooltip__description"
            }
            , [this._v("\n\t\t\t\t\t\t\tCustomer orders a taxi through \n\t\t\t\t\t\t\t"), s("span", {
                staticClass: "tooltip__description_color"
            }
            , [this._v("\n\t\t\t\t\t\t\t\t“Partner app 1”\n\t\t\t\t\t\t\t")]), s("br"), this._v("\n\t\t\t\t\t\t\tthat doesn't work in that location\n\t\t\t\t\t\t")]), s("div", {
                staticClass: "tooltip__buttons"
            }
            , [s("button", {
                staticClass: "tooltip__next"
            }
            , [this._v("\n\t\t\t\t\t\t\t\tNext\n\t\t\t\t\t\t\t")]), s("button", {
                staticClass: "tooltip__cancel"
            }
            , [this._v("\n\t\t\t\t\t\t\t\tCancel\n\t\t\t\t\t\t\t")])])])]), s("div", {
                staticClass: "tooltip__arrow"
            }
            )])
        }
        , function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("p", [this._v("\n\t\t\t\t\t\tЧерез приложение "), s("br"), this._v("\n\t\t\t\t\t\tили такси-бота\n\t\t\t\t\t")])
        }
        , function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("p", [this._v("\n\t\t\t\t\t\tДеньги отправляются "), s("br"), this._v("\n\t\t\t\t\t\tнапрямую исполнителю\n\t\t\t\t\t")])
        }
        , function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("div", {
                staticClass: "text"
            }
            , [s("img", {
                attrs: {
                    src: a("x8fk")
                }
            }
            ), s("p", [this._v("Лондон")])])
        }
        , function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("ul", [s("li", [this._v("Партнёрское приложение")]), s("li", [this._v("Такси-бот")])])
        }
        , function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("div", {
                staticClass: "text"
            }
            , [s("img", {
                attrs: {
                    src: a("5r9R")
                }
            }
            ), s("p", [this._v("Нью Йорк")])])
        }
        , function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("ul", [s("li", [this._v("ПО диспетчеризации иностранной такси")])])
        }
        , function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("div", {
                staticClass:"step-5", attrs: {
                    "data-step": "3|6"
                }
            }
            , [s("img", {
                attrs: {
                    src: a("H3me")
                }
            }
            )])
        }
        , function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("p", [this._v("\n\t\t\t\t\t\tЗаказы отдаются в кредит. При"), s("br"), this._v("\n\t\t\t\t\t\tдостижении кредитного лимита"), s("br"), this._v("\n\t\t\t\t\t\tпроисходит автоматическое"), s("br"), this._v("\n\t\t\t\t\t\tпополнение с кредитной карты"), s("br"), this._v("\n\t\t\t\t\t\tкомпании\n\t\t\t\t\t")])
        }
        , function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("p", [this._v("\n\t\t\t\t\t\tКомиссия передающей стороны"), s("br"), this._v("\n\t\t\t\t\t\tзачисляется на баланс"), s("br"), this._v("\n\t\t\t\t\t\tавтоматический в течение"), s("br"), this._v("\n\t\t\t\t\t\t24 часов после получения"), s("br"), this._v("\n\t\t\t\t\t\tоплаты от исполнителя\n\t\t\t\t\t")])
        }
        , function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("p", [s("b", [this._v("Способ оплаты 2")]), s("br"), this._v("\n\t\t\t\t\t\tналичными таксисту\n\t\t\t\t\t")])
        }
        ];
        e._withStripped=!0;
        var n= {
            render: e, staticRenderFns: i
        }
        ;
        s.a=n
    }
    , dPaJ:function(t, s, a) {
        "use strict";
        var e=a("EerU");
        s.a= {
            components: {
                "app-single-question": e.a
            }
        }
    }
    , "fE+W":function(t, s, a) {
        (t.exports=a("FZ+f")(!1)).push([t.i, ".about-us-problems{background-color:#b9688a;padding:90px 0 160px}.about-us-problems__bg{position:absolute;height:100%;width:100%;-o-object-fit:contain;object-fit:contain}@media screen and (max-width:767px){.about-us-problems{padding:20px 0}}", ""])
    }
    , gmQo:function(t, s, a) {
        "use strict";
        s.a= {
            data:function() {
                return {
                    listOpen:!1, langList:[ {
                        name: "usa", image: a("5r9R")
                    }
                    , {
                        name: "uk", image: a("x8fk")
                    }
                    ], currentLang:null
                }
            }
            , created:function() {
                this.currentLang=this.langList[1]
            }
            , methods: {
                open:function() {
                    this.listOpen=!this.listOpen
                }
                , active:function(t) {
                    this.currentLang=t, this.listOpen=!1
                }
            }
        }
    }
    , i70V:function(t, s, a) {
        "use strict";
        s.a= {
            props: {
                question: {
                    type: String
                }
                , questionClasses: {
                    type: String
                }
                , q_date: {
                    type: String
                }
                , answer: {
                    type: String
                }
                , a_date: {
                    type: String
                }
                , to_next_node: {
                    type: String
                }
                , to_next_class: {
                    type: String
                }
            }
            , data:function() {
                return {
                    wait:[ {
                        question: this.question, q_date: this.q_date, answer: this.answer, a_date: this.a_date, transition: "", show: !1
                    }
                    ], qClassess:this.questionClasses, messages:[], next:this.to_next_node, wowContainer:null
                }
            }
            , methods: {
                active:function() {
                    this.wait[0].transition="animation-delay: "+(20*this.wait[0].question.length+400)+"ms", this.messages[0]=this.wait[0];
                    var t=this.$el.querySelector("."+this.to_next_node);
                    t&&(t.style.animationDelay=20*this.wait[0].question.length+500+"ms")
                }
            }
            , created:function() {
                var t=a("nu+r").WOW;
                this.wowContainer=document.body.offsetWidth>1200&&window.innerHeight>900?".fullpage":null, this.to_next_class&&this.to_next_node&&new t( {
                    boxClass:this.to_next_node, animateClass:this.to_next_class, offset:0, mobile:!0, live:!0, callback:function(t) {}
                    , scrollContainer: this.wowContainer
                }
                ).init()
            }
        }
    }
    , jpGv:function(t, s, a) {
        (t.exports=a("FZ+f")(!1)).push([t.i, ".arrow--down{position:fixed;display:none;width:32px;width:2rem;height:32px;height:2rem;bottom:24px;bottom:1.5rem;left:24px;left:1.5rem;-webkit-animation-name:bounty;animation-name:bounty;-webkit-animation-duration:2s;animation-duration:2s;-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite;-webkit-transition:-webkit-transform 1s ease;transition:-webkit-transform 1s ease;transition:transform 1s ease;transition:transform 1s ease,-webkit-transform 1s ease}.arrow--down:hover{cursor:pointer;-webkit-animation-name:unset;animation-name:unset;-webkit-transform:scale(1.2);transform:scale(1.2)}@media screen and (min-width:1200px) and (min-height:900px){.arrow--down{display:block}}@-webkit-keyframes bounty{0%,to{-webkit-transform:scale(1);transform:scale(1)}50%{-webkit-transform:scale(1.2);transform:scale(1.2)}}@keyframes bounty{0%,to{-webkit-transform:scale(1);transform:scale(1)}50%{-webkit-transform:scale(1.2);transform:scale(1.2)}}", ""])
    }
    , kNwo:function(t, s, a) {
        var e=a("3Os3");
        "string"==typeof e&&(e=[[t.i, e, ""]]), e.locals&&(t.exports=e.locals);
        a("rjj0")("67dd69e1", e, !1, {
            sourceMap: !1
        }
        )
    }
    , kidr:function(t, s, a) {
        t.exports=a.p+"img/elements@2x.4e03e62.png"
    }
    , lL8I:function(t, s, a) {
        (t.exports=a("FZ+f")(!1)).push([t.i, ".about-us-using{background-color:#2b1829;padding:90px 0 160px}@media screen and (max-width:767px){.about-us-using{padding:20px 0}}", ""])
    }
    , mq1t:function(t, s, a) {
        "use strict";
        s.a= {}
    }
    , mup7:function(t, s, a) {
        var e=a("VBPW");
        "string"==typeof e&&(e=[[t.i, e, ""]]), e.locals&&(t.exports=e.locals);
        a("rjj0")("5c036544", e, !1, {
            sourceMap: !1
        }
        )
    }
    , oT4F:function(t, s, a) {
        "use strict";
        var e=a("xX5O"), i=a("t3Uk"), n=!1;
        var o=function(t) {
            n||a("I7D9")
        }
        , r=a("VU/8")(e.a, i.a, !1, o, null, null);
        r.options.__file="components/about-us/AppAboutProblem.vue", s.a=r.exports
    }
    , pwtO:function(t, s, a) {
        t.exports=a.p+"img/about-us-problems.b633a88.png"
    }
    , q55T:function(t, s, a) {
        "use strict";
        var e=a("5GZ6"), i=a("1f8K"), n=!1;
        var o=function(t) {
            n||a("WbMt")
        }
        , r=a("VU/8")(e.a, i.a, !1, o, null, null);
        r.options.__file="components/about-us/AppAboutUsing.vue", s.a=r.exports
    }
    , r3k2:function(t, s, a) {
        t.exports=a.p+"img/taxi2.f3868f1.svg"
    }
    , ruQK:function(t, s, a) {
        "use strict";
        var e=function() {
            var t=this, s=t.$createElement, a=t._self._c||s;
            return a("div", {
                directives:[ {
                    name:"wow", rawName:"v-wow", value: {
                        animateClass: "chat_show", callback: t.active
                    }
                    , expression:"{'animateClass': 'chat_show', 'callback': active}"
                }
                ], staticClass:"chat"
            }
            , [t._l(t.messages, function(s, e) {
                return[a("div", {
                    key: "block-"+e, staticClass: "chat__block"
                }
                , [a("p", {
                    directives:[ {
                        name: "written", rawName: "v-written"
                    }
                    ], staticClass:"chat__question wow written", attrs: {
                        "data-text": s.question
                    }
                }
                ), a("p", {
                    staticClass: "chat__date"
                }
                , [t._v("\n\t\t\t\t"+t._s(s.question.lenght?s.q_date:"")+"\n\t\t\t")])]), s.typing?a("div", {
                    key: "typing-"+e, staticClass: "typing"
                }
                , [a("span", {
                    staticClass: "typing__circle"
                }
                ), a("span", {
                    staticClass: "typing__circle"
                }
                ), a("span", {
                    staticClass: "typing__circle"
                }
                )]):t._e(), s.typing?t._e():a("div", {
                    key: "message-"+e, staticClass: "chat__block chat__block_show animated", style: s.transition
                }
                , [a("p", {
                    staticClass:"chat__answer", domProps: {
                        innerHTML: t._s(s.answer)
                    }
                }
                ), a("p", {
                    staticClass: "chat__date"
                }
                , [t._v(t._s(s.a_date))])])]
            }
            )], 2)
        }
        ;
        e._withStripped=!0;
        var i= {
            render: e, staticRenderFns: []
        }
        ;
        s.a=i
    }
    , s6Fc:function(t, s, a) {
        t.exports=a.p+"img/iphone.09327be.png"
    }
    , smZX:function(t, s, a) {
        "use strict";
        var e=a("mq1t"), i=a("O+Hr"), n=a("VU/8")(e.a, i.a, !1, null, null, null);
        n.options.__file="components/AppFooter.vue", s.a=n.exports
    }
    , t3Uk:function(t, s, a) {
        "use strict";
        var e=function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("div", {
                staticClass:"about-us-problems", attrs: {
                    id: "app--about-us-problems"
                }
            }
            , [s("div", {
                staticClass: "container"
            }
            , [s("div", {
                staticClass: "row position-relative"
            }
            , [s("img", {
                staticClass:"about-us-problems__bg", attrs: {
                    src: a("gjRl")
                }
            }
            ), this._m(0), s("div", {
                staticClass: "col-lg-7 col-md-12"
            }
            , [s("div", {
                staticClass: "about-us-problems__message"
            }
            , [s("app-single-question", {
                attrs: {
                    question: "Pretty cool! But I'm not a taxi company, what's in it for me?", questionClasses: "chat__question--dark", q_date: "today, 4:28 pm", answer: "Let's say you're an avid traveller - you've arrived at the\nairport of your destination, you need a ride and you face\nthe following issues:", a_date: "today, 4:29 pm", to_next_node: "chat__about-us-problems", to_next_class: "chat__about-us-problems_show"
                }
            }
            , [s("ul", {
                staticClass: "chat__about-us-problems"
            }
            , [s("li", [s("svg", {
                staticClass: "chat__svg"
            }
            , [s("use", {
                attrs: {
                    "xlink:href": "#cancel"
                }
            }
            )]), s("span", [this._v("\n\t\t\t\t\t\t\t\t\tThe taxis near the airport's exits look suspicious;\n\t\t\t\t\t\t\t\t")])]), s("li", [s("svg", {
                staticClass: "chat__svg"
            }
            , [s("use", {
                attrs: {
                    "xlink:href": "#cancel"
                }
            }
            )]), s("span", [this._v("\n\t\t\t\t\t\t\t\t\tNone of taxi apps you have work there;\n\t\t\t\t\t\t\t\t")])]), s("li", [s("svg", {
                staticClass: "chat__svg"
            }
            , [s("use", {
                attrs: {
                    "xlink:href": "#cancel"
                }
            }
            )]), s("span", [this._v("\n\t\t\t\t\t\t\t\t\tIf you use the taxis near the exit, you're not sure you won't be charged more than you can afford;\n\t\t\t\t\t\t\t\t")])]), s("li", [s("svg", {
                staticClass: "chat__svg"
            }
            , [s("use", {
                attrs: {
                    "xlink:href": "#cancel"
                }
            }
            )]), s("span", [this._v("\n\t\t\t\t\t\t\t\t\tIf you decide to use an app, you'll have to spend time to make a research on them and even then you can't be sure.\n\t\t\t\t\t\t\t\t")])]), s("li", {
                staticClass: "w-100"
            }
            , [s("a", {
                staticClass:"signup-btn signup-btn_yellow position-relative", attrs: {
                    href: "/register1"
                }
            }
            , [this._v("\n\t\t\t\t\t\t\t\t\tOk, what’s next?\n\t\t\t\t\t\t\t\t\t"), s("svg", {
                staticClass: "signup-btn__arrow"
            }
            , [s("use", {
                attrs: {
                    "xlink:href": "#login-arrow"
                }
            }
            )])])])])])], 1)])])])])
        }
        , i=[function() {
            var t=this.$createElement, s=this._self._c||t;
            return s("div", {
                staticClass: "col-5 hide-md d-flex align-items-center"
            }
            , [s("img", {
                staticClass:"mw-100", attrs: {
                    src: a("pwtO")
                }
            }
            )])
        }
        ];
        e._withStripped=!0;
        var n= {
            render: e, staticRenderFns: i
        }
        ;
        s.a=n
    }
    , tnYm:function(t, s, a) {
        t.exports=a.p+"videos/about-us.6d91875.mp4"
    }
    , vAh8:function(t, s, a) {
        (t.exports=a("FZ+f")(!1)).push([t.i, ".about-us-success{background-color:#00ac90;padding:90px 0 160px}.about-us-success__bg{position:absolute;height:100%;width:100%;-o-object-fit:contain;object-fit:contain}@media screen and (max-width:767px){.about-us-success{padding:20px 0}}", ""])
    }
    , wYaz:function(t, s, a) {
        (t.exports=a("FZ+f")(!1)).push([t.i, ".about-us-header{margin-top:60px;padding-bottom:60px}@media screen and (max-width:767px){.about-us-header{-ms-flex-wrap:wrap-reverse;flex-wrap:wrap-reverse;margin-top:0}}.about-us-header__image{position:relative;text-align:center;z-index:2}.about-us-header__image img{width:100%;max-width:280px;height:575px}.about-us-header__image video{position:absolute;background-color:#000;left:50%;-webkit-transform:translateX(-50%);transform:translateX(-50%);top:15px;width:244px;max-width:calc(100% - 66px);border-radius:25px;height:530px}.about-us-header__back{position:absolute;top:0;left:0}", ""])
    }
    , x8fk:function(t, s, a) {
        t.exports=a.p+"img/uk@2x.6106827.png"
    }
    , xX5O:function(t, s, a) {
        "use strict";
        var e=a("EerU");
        s.a= {
            components: {
                "app-single-question": e.a
            }
        }
    }
    , zZxM:function(t, s, a) {
        "use strict";
        Object.defineProperty(s, "__esModule", {
            value: !0
        }
        );
        var e=a("OqR1"), i=a("CYmg"), n=!1;
        var o=function(t) {
            n||a("5nBj")
        }
        , r=a("VU/8")(e.a, i.a, !1, o, null, null);
        r.options.__file="pages/about-us/index.vue", s.default=r.exports
    }
}

);